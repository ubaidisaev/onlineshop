Old ci/cd

image: node:14.17.5-alpine

pages:
    stage: deploy
    script:
        - npm install
        - npm run build
        - mkdir .public
        - cp -r * .public
        - mv .public public
    artifacts:
        paths:
            - public
    only:
        - main
